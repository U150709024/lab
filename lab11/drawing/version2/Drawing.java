package drawing.version2;

import java.util.ArrayList;

import shapes.*;


public class Drawing {
	
	/*private ArrayList<Circle> circles = new ArrayList<Circle>();
	private ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();*/
	private ArrayList<Object> shapes = new ArrayList<Object>();
	
	public double calculateTotalArea(){
		double totalArea = 0;
		for (Object sh : shapes){
			if(sh instanceof Circle) {
				totalArea += ((Circle)sh).area();
			} else if(sh instanceof Rectangle){
				totalArea +=((Rectangle)sh).area();
			}else if(sh instanceof Square){
				totalArea +=((Square)sh).area();
			}
		}

		/*for (Circle circle : circles){
			totalArea += circle.area();    
		}
		
		for (Rectangle rect : rectangles){            
			totalArea += rect.area();     
		}	*/
		return totalArea;
	}
	
	public void addShape(Object s){
		addShape(s);
	}
}
