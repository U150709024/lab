package drawing.version3;

import java.util.ArrayList;

import shapes.Shape;

public class Drawing {
	
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	//private ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();
	
	public double calculateTotalArea(){
		double totalArea = 0;
		for(Shape shape : shapes){
			totalArea +=shape.area(); //abstract classta area tanımlı olduğu için casting gerek kalmadı
		} //sadece common methodlar bu şekilde çağırılabilir

		/*for (Circle circle : circles){
			totalArea += circle.area();    
		}
		
		for (Rectangle rect : rectangles){            
			totalArea += rect.area();     
		}	*/
		return totalArea;
	}
	public void addShape(Shape s){
		shapes.add(s);
	}
	/*public void addCircle(Circle c) {
		circles.add(c);
	}
	
	public void addRectangle(Rectangle r) {
		rectangles.add(r);
	}*/
}
