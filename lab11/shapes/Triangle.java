package shapes;

public class Triangle extends Shape{
    int side;
    int height;
    public Triangle(int side,int height){
        this.side=side;
        this.height=height;
    }
    @Override
    public double area(){
        return (side*height)/2;
    }
}
